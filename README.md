# Traccar Telegram Bot

Is a (as the name suggest) bot written in Python (at least 3.8 and above) for [Telegram Messenger](https://telegram.org/).
It connects to your instance of [Traccar](https://www.traccar.org/) and let you do some basic operation like retrieve the location of your devices or send commands.

In order to use it is mandatory to own an account on a Traccar Server.
Also you need to generate or retrieve you API Key.
The API Key is a random string that identifies you on the server, this avoid putting username and password somewhere, also you can change it whenever you want if you feel is has been disclosed.

Before installation be sure to have:

- A Telegram bot API Key, you could create a bot and retrieve the API Key by writing to [botfather](https://t.me/botfather/)
- A Traccar API Key, in order to communicate with the server (last field in the first [image](https://www.traccar.org/documentation/user-management/))
- Python 3 installed with pip (Python 3.8 and above is required due to [python-telegram-bot version](https://pypi.org/project/python-telegram-bot/))

[[_TOC_]]

## Installation

I strongly suggest to use virtualenv to run the bot, but feel free to do what you want, the only differences would be if you plan to start the bot as system service.

Move into your choosen directory and issue the git clone command

  `git clone https://gitlab.com/argaar/traccar-telegram-bot.git`

now on your directory you should find some files, let's dig a bit about them:

- data.db.example
  rename this in data.db, it is the sqlite3 database used to store user's apikey

- parameters.conf.example
  rename this in parameters.conf and change the parameters inside

- traccat-bot.py
  this is the main python file, it's the bot itself

- requirements.txt
  pip library list, you need to install those library in order to use the bot, do it with the command
  
  `pip install -r requirements.txt`

## Configuration

You shoud edit the file _parameters.txt_ accordingly to the way you want to run the bot.
If you plan to launch it as a systemd service you probably want to put the absolute path of files in the file
Otherwise, if you plan to launch as shell command inside the virtualenv (maybe with the nohup) there's a good chance it will work straightforward.

More on Traccar configuration could be found at the [project documentation](https://www.traccar.org/documentation/)

## Install as system service

If you plan to launch it as a daemon, you should create a Systemd unit file (tipically as /etc/systemd/system/traccar-telegram-bot.service), and maybe this could help you:

```desktop
[Unit]
Description=Traccar Telegram Bot
After=multi-user.target

[Service]
Type=simple
User=[LINUX_USER]
Group=[LINUX_GROUP]
ExecStart=[PATH_TO_PYTHON] [PATH_TO_BOT.py] -c [PATH_TO_CONFIGURATION_FILE] -l [PATH_TO_LOGFILE]
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

After successfully created the unit file, remeber to reload the systemd unit with `systemctl daemon-reload`, at this point you should be able to see the stopped service with `systemctl status traccar-telegram-bot`
If you've already configured the parameters.conf you could start the service with `systemctl start traccar-telegram-bot`, please remember to enable the service with `systemctl enable traccar-telegram-bot`
otherwise the script won't run automatically at the next boot

**NB: be aware that if you use the above Systemd Service Unit file, you should specify the full path of database in configuration file since the unitfile has no WorkingDirectory value

## Bot Commands

Since you need to obtain an API Key from Telegram, you also could configure the bot to show users some predefined commands (that this bot uses).
When the BotFathers asks you for a list of them you could simply copy/paste the block below:

```desktop
start - Start the initial setup
register - Register a Traccar API Key (new or change the stored one)
location - Retrieve location of a device you own
sendcmd - Send pre-configured command to a device you own
geofences - Create or delete/list geofences for a device you own
status - Get status data about a device you own
```

Of course you could remove some of them from the list but **keep in mind that the bot will reply to them anyway, they're hidden to users but active in the script.**

## Using

The bot provides some commands as described above:

- start
  this is the default command if you approach a bot the first time, it provides you the ability to configure the Traccar API Key

- register
  In case of API Key changed or if you skip the "start" command you could send the (new) API Key. If a key is already present in the database, it will be overwrite

- location
  returns the location and the last update time of the device you choose, along with a thumbnail of the map taht you could click to explore more in detail
  
- sendcmd
  Traccar let you configure some command you could send to your device if it support remote controlling, choose the one you want to send.
  **Remember that some devices has a scheduled time to contact Traccar, so your command could be sent immediately or put in a queue waiting for delivery**

- geofences
  the command let you create or delete a geofence.
  The main purpose is to let the user to create a (temporary) geofence in order to act as an alarm in case of attempted robbery  
  Due to Traccar architecture, when a geofence is created, the bot automatically associate it with the device you choose
  For the same reason, and since there's no way to know if a geofence is in use by more than one device, when you delete it, you simply disassociate it from the device.
  
  _You could also start creating a geofence, simply sharing your position with the bot_
  
- status
  provides some (few) info about the device you choose

## Help me, it doesn't work

No problem, you could start investigatin the issue you're facing by add the "-d" option when launching the bot (eg: python traccar-bot.py -c parameters.conf -l bot_log.log **-d**),
this switch will activate the debug mode and fill your log file with a lot on informations. Look at all the lines to see if something is going wrong for you.

Additional you could open an issue here in GitLab asking for help.
Traccar ApiKey, Telegram Token and Session Cookie are intentionally obfuscated in the log file for privacy concerns, but
**Device Position is NOT**. So if you're copying/pasting the log file on GitLab please remember to redact coordinates as well

## Warranty

Since this bot has access to your devices and their position, even if there's no direct relatonship between you and your device, disclosing of the API Key or some other bug could let someone **know where you and your device (and you car/bike/etc.) are**

THE FILES (FROM NOW, "THE SOFTWARE") ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
