#!/usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = 'Davide "argaar" Foschi'
#
# Version 1.0.4

import argparse, configparser, json, logging, os, random, requests, sqlite3, string
from datetime import datetime, timedelta, timezone
from dateutil import parser
from telegram import (KeyboardButton, Location, ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.error import (TelegramError)
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

""" Define global variables """
logger = None
DEBUG = False
DB_HOST = None
TRACCAR_URL = None
TRACCAR_TIMEOUT = 0
USER_STATUSES = {}

###### START CORE FUNCTS
def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "{}" caused error "{}"'.format( update, context.error ))

def get_config(config_file):
    config = configparser.RawConfigParser()

    config.add_section('sqlite')
    config.set('sqlite', 'db', 'data.db')

    config.add_section('telegram')
    config.set('telegram', 'token', 'YOURTOKEN')

    config.add_section('traccar')
    config.set('traccar', 'host', 'localhost')
    config.set('traccar', 'port', '8082')
    config.set('traccar', 'schema', 'http')
    config.set('traccar', 'timeout', '60')
    if DEBUG: logger.debug('get_config: Config object created')
    if not os.path.exists(config_file):
        logger.error('get_config.loadfile.exception: file not exists {}'.format( config_file ))
    else:
        config.read(config_file)
        if DEBUG: logger.debug('get_config: Config object loaded from file')
    return config

def end_user_session(text, update, context):
    if DEBUG: logger.debug('end_user_session: Clear called by user {}'.format( update.message.chat.id ))
    context.user_data.clear()
    if DEBUG: logger.debug('end_user_session: text to send is "{}"'.format( text ))
    update.message.reply_text(text=text, parse_mode='HTML', reply_markup=ReplyKeyboardRemove())

def split_list (list, x):
    return [list[i:i+x] for i in range(0, len(list), x)]
###### STOP CORE FUNCTS

###### START STEP "NEW/REGISTER"
def start(update, context):
    context.user_data['uid'] = update.message.chat.id
    if DEBUG: logger.debug('start: Start called by user {}'.format( update.message.chat.id ))
    apikey = retrieve_apikey(context)
    if apikey is None:
        if DEBUG: logger.debug('start: NO apikey')
        add_traccar_key(update, context)
    else:
        if DEBUG: logger.debug('start: apikey OK')
        text = 'You already have an apikey, use other commands to interact with the server.'
        end_user_session(text, update, context)

def add_traccar_key(update, context):
    if DEBUG: logger.debug('add_traccar_key: called by user {}'.format( update.message.chat.id ))
    context.user_data['status'] = 'new'
    text='Please insert the api key retrieved from Traccar server.\n' \
        'You can get it from the account panel, lookin at the end of "Permissions" tab, if the field is void, click the icon next to it in order to generate\n\n' \
        '** REMEBER TO SAVE YOUR PROFILE WITH THE NEW KEY BEFORE LEAVE THE SITE **'
    buttons = [
            [KeyboardButton(text='END')]
        ]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=text, reply_markup=keyboard)

def save_apikey(update, context):
    if DEBUG: logger.debug('save_apikey: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    text = None
    if DEBUG: logger.debug('save_apikey: Received Key {}'.format( msg[:-8]+"XXXXXXX" ))
    if perform_auth(context, msg):
        with sqlite3.connect(DB_HOST) as conn:
            cur = conn.cursor()
            try:
                cur.execute("REPLACE INTO users (tg_id, traccar_apikey) VALUES (?, ?)", (context.user_data['uid'], msg) )
                if cur.lastrowid is not None:
                    text = 'APIKEY saved successfully: '+msg
                    if DEBUG: logger.debug('save_apikey: apikey saved')
                else:
                    text = 'There was an error adding your key.'
                    if DEBUG: logger.debug('save_apikey: error in adding api key')
            except OperationalError as op_err:
                logger.error('save_apikey.replace.exception: db_err {}, db: {}'.format( op_err, DB_HOST ))
    else:
        text = 'Your APIKEY seems to be invalid, check if it has been saved on the server and if you miss some characters.'
        if DEBUG: logger.debug('save_apikey: auth not done')
    end_user_session(text, update, context)
###### STOP STEP "NEW/REGISTER"

###### START STEP "LOCATION"
def location(update, context):
    if DEBUG: logger.debug('location: called by user {}'.format( update.message.chat.id ))
    context.user_data['uid'] = update.message.chat.id
    if retrieve_apikey(context) is None:
        if DEBUG: logger.debug('location: no apikey')
        start(update, context)
    else:
        context.user_data['status'] = 'location'
        if retrieve_devices(update, context) is not None:
            devices = context.user_data['devices']
            if DEBUG: logger.debug('location: devices retrieved {}'.format( devices ))
            if len(devices) > 1:
                devices_buttons = []
                for device in devices:
                    text = device['name']
                    devices_buttons.append(KeyboardButton(text=text))
                    if DEBUG: logger.debug('location: created device_button with device name {}'.format( text ))
                text='Please select the device.'
                buttons = split_list(devices_buttons, 4)
                buttons.append([KeyboardButton(text='END')])
                keyboard = ReplyKeyboardMarkup(buttons)
                update.message.reply_text(text=text, reply_markup=keyboard)
            elif len(devices) == 1:
                if DEBUG: logger.debug('location: only 1 device retrieved {}'.format( devices ))
                retrieve_location(update, context)
                #We just "pass" because if there's just one device we want to count it in the next step
                #and retrieve the only attribute we're looking for
            else:
                if DEBUG: logger.debug('location: no device retrieved')
                update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
        else:
            logger.error('location.retrievedevice.exception: retrieve_device(): False')
            text='There was an error retrieving your devices'
            end_user_session(text, update, context)

def retrieve_location(update, context, data_return=False):
    if DEBUG: logger.debug('retrieve_location: called by user {}'.format( update.message.chat.id ))
    if DEBUG: logger.debug('retrieve_location: data_return is {}'.format( data_return ))
    msg = update.message.text
    devices = context.user_data['devices']
    dev_last_position = 0
    if len(devices) == 1:
        dev_last_position = devices[0]['last_position']
    else:
        for device in devices:
            if device['name'] == msg:
                dev_last_position = device['last_position']
    if DEBUG: logger.debug('retrieve_location: get last position {}'.format( dev_last_position ))
    position = call_traccar_api('location', context, dev_last_position)
    if position is not None:
        if DEBUG: logger.debug('retrieve_location: got position {}'.format( position))
        if data_return:
            if DEBUG: logger.debug('retrieve_location: return position attributes to function')
            position_data = {}
            supported_attributes = ['alarm', 'ignition', 'motion', 'gpsStatus', 'power', 'battery', 'odometer', 'sat']
            for item in position['attributes']:
                if item in supported_attributes:
                    position_data[item] = position['attributes'][item]
            return position_data
        else:
            if DEBUG: logger.debug('retrieve_location: print location data to user')
            lat = position['latitude']
            lng = position['longitude']
            addr = position['address']
            position_ts = position['serverTime']
            location = Location(lng, lat)
            update.message.reply_location(location=location)
            update.message.reply_text(text='Position updated at: '+position_ts)
            text = 'Address not available'
            if addr is not None:
                text = addr
    else:
        text='There was an error retrieving location data for device: {}'.format(msg)
        if DEBUG: logger.debug('retrieve_location: no position')
    end_user_session(text, update, context)
###### STOP STEP "LOCATION"

###### START STEP "GEOFENCES"
def geofences(update, context):
    if DEBUG: logger.debug('geofences: called by user {}'.format( update.message.chat.id ))
    context.user_data['uid'] = update.message.chat.id
    if retrieve_apikey(context) is None:
        start(update, context)
    else:
        context.user_data['status'] = 'geofences'
        context.user_data['step'] = 'choice'
        text='Whould you like to create or delete a Geofence?'
        buttons = [
                [KeyboardButton(text='Create Geofence'),KeyboardButton(text='Delete Geofences')],
                [KeyboardButton(text='END')]
            ]
        keyboard = ReplyKeyboardMarkup(buttons)
        update.message.reply_text(text=text, reply_markup=keyboard)

def manage_geofences(update, context):
    if DEBUG: logger.debug('manage_geofences: called by user {}'.format( update.message.chat.id ))
    context.user_data['status'] = 'geofences'
    context.user_data['step'] = 'retrieve'
    if retrieve_devices(update, context) is not None:
        if DEBUG: logger.debug('manage_geofences: devices retrieve ok {}'.format( context.user_data['devices'] ))
        devices = context.user_data['devices']
        if len(devices) > 1:
            devices_buttons = []
            for device in devices:
                text = device['name']
                devices_buttons.append(KeyboardButton(text=text))
            text='Please select the device.'
            buttons = split_list(devices_buttons, 4)
            buttons.append([KeyboardButton(text='END')])
            keyboard = ReplyKeyboardMarkup(buttons)
            update.message.reply_text(text=text, reply_markup=keyboard)
        elif len(devices) == 1:
                retrieve_geofences(update, context)
                #We just "pass" because if there's just one device we want to count it in the next step
                #and retrieve the only attribute we're looking for
        else:
            update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
    else:
        logger.error('manage_geofences.retrievedevice.exception: retrieve_device(): False')
        text='There was an error retrieving your devices'
        end_user_session(text, update, context)

def retrieve_geofences(update, context):
    if DEBUG: logger.debug('retrieve_geofences: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    context.user_data['status'] = 'geofences'
    context.user_data['step'] = 'delete'
    devices = context.user_data['devices']
    dev_id = 0
    if len(devices) == 1:
        dev_id = devices[0]['id']
        context.user_data['geofence_dev_id'] = devices[0]['id']
        context.user_data['geofence_dev_name'] = devices[0]['name']
    else:
        for device in devices:
            if device['name'] == msg:
                dev_id = device['id']
                context.user_data['geofence_dev_id'] = device['id']
                context.user_data['geofence_dev_name'] = device['name']
    if DEBUG: logger.debug('retrieve_geofences: getting geofences for device id {}, name {}'.format( context.user_data['geofence_dev_id'], context.user_data['geofence_dev_name'] ))
    geofences_data = call_traccar_api('geofences', context, dev_id)
    if geofences_data is not None:
        if DEBUG: logger.debug('retrieve_geofences: geofence(s) retrieved {}'.format( geofences_data ))
        if len(geofences_data) >= 1:
            context.user_data['geofences'] = geofences_data
            geofences_buttons = []
            for geofence in geofences_data:
                text = geofence['name']
                geofences_buttons.append(KeyboardButton(text=text))
            text='Please select the Geofence you would to delete.'
            buttons = [
                    geofences_buttons,
                    [KeyboardButton(text='END')]
                ]
            keyboard = ReplyKeyboardMarkup(buttons)
            update.message.reply_text(text=text, reply_markup=keyboard)
        else:
            update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
    else:
        logger.error('retrieve_geofences.retrievegeofence.exception: retrieve_geofence(): False')
        text='There was an error retrieving geofences data for device: {}'.format(dev_id)
        end_user_session(text, update, context)

def create_geofence(update, context):
    if DEBUG: logger.debug('create_geofence: called by user {}'.format( update.message.chat.id ))
    text='In order to create Geofence, send me the position.'
    buttons = [
            [KeyboardButton(text='Send Location', request_location=True)],
            [KeyboardButton(text='END')]
        ]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=text, reply_markup=keyboard)

def create_geofence_device(update, context):
    if DEBUG: logger.debug('create_geofence_device: called by user {}'.format( update.message.chat.id ))
    if retrieve_apikey(context) is None:
        start(update, context)
    else:
        context.user_data['status'] = 'geofences'
        context.user_data['step'] = 'radius'
        if retrieve_devices(update, context) is not None:
            if DEBUG: logger.debug('create_geofence_device: devices retrieve ok {}'.format( context.user_data['devices'] ))
            devices = context.user_data['devices']
            if len(devices) > 1:
                devices_buttons = []
                for device in devices:
                    text = device['name']
                    devices_buttons.append(KeyboardButton(text=text))
                text='Please select the device.'
                buttons = split_list(devices_buttons, 4)
                buttons.append([KeyboardButton(text='END')])
                keyboard = ReplyKeyboardMarkup(buttons)
                update.message.reply_text(text=text, reply_markup=keyboard)
            elif len(devices) == 1:
                create_geofence_radius(update, context)
                #We just "pass" because if there's just one device we want to count it in the next step
                #and retrieve the only attribute we're looking for
            else:
                update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
        else:
            logger.error('geofence.retrievedevice.exception: retrieve_device(): False')
            text='There was an error retrieving your devices'
            end_user_session(text, update, context)

def create_geofence_radius(update, context):
    if DEBUG: logger.debug('create_geofence_radius: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    context.user_data['status'] = 'geofences'
    context.user_data['step'] = 'name'
    devices = context.user_data['devices']
    if len(devices) == 1:
        context.user_data['geofence_dev_id'] = devices[0]['id']
        context.user_data['geofence_dev_name'] = devices[0]['name']
    else:
        for device in devices:
            if device['name'] == msg:
                context.user_data['geofence_dev_id'] = device['id']
                context.user_data['geofence_dev_name'] = device['name']
    text='Choose the radius size of the circle centered at your position (in meters).'
    buttons = [
            [KeyboardButton(text='100'),KeyboardButton(text='200'),KeyboardButton(text='300'),KeyboardButton(text='400')],
            [KeyboardButton(text='500'),KeyboardButton(text='750'),KeyboardButton(text='1000'),KeyboardButton(text='2000')],
            [KeyboardButton(text='END')]
        ]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=text, reply_markup=keyboard)

def create_geofence_name(update, context):
    if DEBUG: logger.debug('create_geofence_name: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    context.user_data['radius'] = msg
    context.user_data['status'] = 'geofences'
    context.user_data['step'] = 'create'
    text='Write a name for this Geofence or let the system generate one for you randomly.'
    buttons = [
            [KeyboardButton(text='Random')],
            [KeyboardButton(text='END')]
        ]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=text, reply_markup=keyboard)

def create_geofence_send(update, context):
    if DEBUG: logger.debug('create_geofence_send: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    if msg != 'Random':
        geo_name = msg
    else:
        geo_name = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
    radius = context.user_data['radius']
    lat = context.user_data['lat']
    lng = context.user_data['lng']
    dev_id = context.user_data['geofence_dev_id']
    dev_name = context.user_data['geofence_dev_name']
    area = "CIRCLE ("+str(lat)+" "+str(lng)+", "+str(radius)+")"
    if DEBUG: logger.debug('create_geofence_send: creating geofence name {} with radius {}, lat {}, lng {} for device devid {}, dev_name'.format( geo_name, radius, lat, lng, dev_id, dev_name ))
    geofence_data = call_traccar_api('create_geofences', context, json.dumps({'name':geo_name, 'area':area}))
    if geofence_data is not None:
        geo_id = geofence_data['id']
        if DEBUG: logger.debug('create_geofence_send: geofence created with id {}'.format( geo_id ))
        geo_assoc = call_traccar_api('assoc_geofence', context, json.dumps({ 'deviceId':dev_id, 'geofenceId': geo_id }))
        if geo_assoc:
            if DEBUG: logger.debug('create_geofence_send: geofence id {} associated with device id {}'.format( geo_id, dev_id ))
            text = 'Successfully created and associated Geofence {} with device {}'.format(geo_name, dev_name)
        else:
            text='There was an error associating geofence: {} for device: {}' \
                 'NB: the Geofence, however, has been created, you can delete it from the Traccar Web Page.'.format(geo_name, dev_name)
            logger.error('create_geofence_send.geofence_data.exception: associategeofence(): False')
    else:
        logger.error('create_geofence_send.geofence_data.exception: creategeofence(): False')
        text='There was an error creating geofence: {} for device: {}'.format(geo_name, dev_name)
    end_user_session(text, update, context)

def delete_geofence(update, context):
    if DEBUG: logger.debug('delete_geofence: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    context.user_data['status'] = 'geofences'
    context.user_data['step'] = 'delete_confirm'
    dev_name = context.user_data['geofence_dev_name']
    geofences = context.user_data['geofences']
    geo_name = ''
    geo_id = 0
    for geofence in geofences:
        if geofence['name'] == msg:
            geo_id = geofence['id']
            geo_name = geofence['name']
            context.user_data['geofence_id'] = geofence['id']
            context.user_data['geofence_name'] = geofence['name']
    text='Are you sure you want to delete Geofence: {} for Device: {}?\n' \
         'NB: since there\'s no way to know if this geofence is already used by some other devices\n' \
         'this actually don\'t delete the geofence itself, but unlink it from the device.'.format(geo_name, dev_name)
    buttons = [
            [KeyboardButton(text='YES')],
            [KeyboardButton(text='END')]
        ]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=text, reply_markup=keyboard)

def delete_geofence_confimation(update, context):
    if DEBUG: logger.debug('delete_geofence_confimation: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    dev_id = context.user_data['geofence_dev_id']
    dev_name = context.user_data['geofence_dev_name']
    geo_id = context.user_data['geofence_id']
    geo_name = context.user_data['geofence_name']
    if DEBUG: logger.debug('delete_geofence_confimation: deleting (disassociating) geofence name {}, id [} from device name {}, id {}'.format( geo_name, geo_id, dev_name, dev_id ))
    geofence_status = call_traccar_api('del_geofences', context, json.dumps({ 'deviceId':dev_id, 'geofenceId': geo_id }))
    if geofence_status:
        text='Successfully delete the Geofence: {} for device: {}'.format(geo_name, dev_name)
        if DEBUG: logger.debug('delete_geofence_confimation: deleted (disassociated) geofence name {} from device name {}'.format( geo_name, dev_name ))
    else:
        text='There was an error deleting geofence: {} for device: {}'.format(geo_name, dev_name)
        logger.error('delete_geofence_confimation.delete_geofence.exception: deletegeofence(): False')
    end_user_session(text, update, context)
###### STOP STEP "GEOFENCES"

###### START STEP "COMMANDS"
def commands(update, context):
    if DEBUG: logger.debug('commands: called by user {}'.format( update.message.chat.id ))
    context.user_data['uid'] = update.message.chat.id
    if retrieve_apikey(context) is None:
        start(update, context)
    else:
        context.user_data['status'] = 'commands'
        context.user_data['step'] = 'retrieve'
        if retrieve_devices(update, context) is not None:
            if DEBUG: logger.debug('commands: devices retrieve ok {}'.format( context.user_data['devices'] ))
            devices = context.user_data['devices']
            if len(devices) > 1:
                devices_buttons = []
                for device in devices:
                    text = device['name']
                    devices_buttons.append(KeyboardButton(text=text))
                text='Please select the device.'
                buttons = split_list(devices_buttons, 4)
                buttons.append([KeyboardButton(text='END')])
                keyboard = ReplyKeyboardMarkup(buttons)
                update.message.reply_text(text=text, reply_markup=keyboard)
            elif len(devices) == 1:
                retrieve_commands(update, context)
                #We just "pass" because if there's just one device we want to count it in the next step
                #and retrieve the only attribute we're looking for
            else:
                update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
        else:
            logger.error('commands.retrievedevice.exception: retrieve_device(): False')
            text='There was an error retrieving your devices'
            end_user_session(text, update, context)

def retrieve_commands(update, context):
    if DEBUG: logger.debug('retrieve_commands: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    devices = context.user_data['devices']
    dev_id = 0
    if len(devices) == 1:
        dev_id = devices[0]['id']
    else:
        for device in devices:
            if device['name'] == msg:
                dev_id = device['id']
    if DEBUG: logger.debug('retrieve_commands: retrieving command(s) from device id {} '.format( dev_id ))
    commands_data = call_traccar_api('commands', context, dev_id)
    if commands_data is not None:
        if DEBUG: logger.debug('retrieve_commands: command(s) retrieved {} '.format( commands_data ))
        if len(commands_data) >= 1:
            context.user_data['status'] = 'commands'
            context.user_data['step'] = 'send'
            context.user_data['command_dev_id'] = dev_id
            context.user_data['command_dev_name'] = msg
            context.user_data['commands'] = commands_data
            commands = commands_data
            commands_buttons = []
            for command in commands:
                text = command['description']
                commands_buttons.append(KeyboardButton(text=text))
            text='Please select the command.'
            buttons = [
                    commands_buttons,
                    [KeyboardButton(text='END')]
                ]
            keyboard = ReplyKeyboardMarkup(buttons)
            update.message.reply_text(text=text, reply_markup=keyboard)
        else:
            update.message.reply_text(text='It seems that you don\'t have any command registered.', reply_markup=ReplyKeyboardRemove())
    else:
        text='There was an error retrieving commands data for device: {}'.format(msg)
        logger.error('retrieve_commands.retrievecommands.exception: retrievecommands(): False')
        end_user_session(text, update, context)

def send_command(update, context):
    if DEBUG: logger.debug('send_command: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    dev_id = context.user_data['command_dev_id']
    dev_name = context.user_data['command_dev_name']
    commands = context.user_data['commands']
    for command in commands:
        if command['description'] == msg:
            command_id = command['id']
    if DEBUG: logger.debug('send_command: sending command name {}, id {} to device name {}, id {}'.format( msg, command_id, dev_name, dev_id ))
    sendcommand_data = call_traccar_api('send_command', context, json.dumps({'id':command_id, 'deviceId':dev_id}))
    if sendcommand_data is not None:
        text = sendcommand_data
        if DEBUG: logger.debug('send_command: sent command with status {}'.format( sendcommand_data ))
    else:
        text='There was an error sending commands data for device: {}'.format(dev_name)
        logger.error('send_command.sendcommand.exception: sendcommand(): False')
    end_user_session(text, update, context)
###### STOP STEP "COMMANDS"

###### START STEP "STATUS"
def status(update, context):
    if DEBUG: logger.debug('status: called by user {}'.format( update.message.chat.id ))
    context.user_data['uid'] = update.message.chat.id
    if retrieve_apikey(context) is None:
        start(update, context)
    else:
        context.user_data['status'] = 'status'
        if retrieve_devices(update, context) is not None:
            if DEBUG: logger.debug('status: device(s) retrieved {}'.format( context.user_data['devices'] ))
            devices = context.user_data['devices']
            if len(devices) > 1:
                devices_buttons = []
                for device in devices:
                    text = device['name']
                    devices_buttons.append(KeyboardButton(text=text))
                text='Please select the device.'
                buttons = split_list(devices_buttons, 4)
                buttons.append([KeyboardButton(text='END')])
                keyboard = ReplyKeyboardMarkup(buttons)
                update.message.reply_text(text=text, reply_markup=keyboard)
            elif len(devices) == 1:
                retrieve_status(update, context)
                #We just "pass" because if there's just one device we want to count it in the next step
                #and retrieve the only attribute we're looking for
            else:
                update.message.reply_text(text='It seems that you don\'t have a device registered.', reply_markup=ReplyKeyboardRemove())
        else:
            logger.error('status.retrievedevice.exception: retrieve_device(): False')
            text='There was an error retrieving your devices'
            end_user_session(text, update, context)

def retrieve_status(update, context):
    if DEBUG: logger.debug('retrieve_status: called by user {}'.format( update.message.chat.id ))
    msg = update.message.text
    devices = context.user_data['devices']
    dev_id = 0
    if len(devices) == 1:
        dev_id = devices[0]['id']
    else:
        for device in devices:
            if device['name'] == msg:
                dev_id = device['id']
    if DEBUG: logger.debug('retrieve_status: retrieving status for device name id {}'.format( dev_id ))
    device_data = call_traccar_api('status', context, dev_id)
    if device_data is not None:
        if DEBUG: logger.debug('retrieve_status: status retrieved for device {}'.format( device_data ))
        text='Name: {}\nStatus: {}\nLast Update: {}\nModel: {}\nIn Geofence: {}'.format(
            device_data['name'],
            device_data['status'],
            datetime.strptime(device_data['lastUpdate'], '%Y-%m-%dT%H:%M:%S.%f%z').strftime('%Y-%m-%dT%H:%M:%SZ'),
            device_data['model'] if device_data['model'] != '' else 'no model specified',
            'YES' if len(device_data['geofenceIds'])>=1 else 'NO')
        try:
            additional_attributes = retrieve_location(update, context, True)
            if DEBUG: logger.debug('retrieve_status: additional attributes retrieved from last location {}'.format( additional_attributes ))
            if additional_attributes is not None:
                text = text + "\n\n-- Additional Info --\n"
                for attrib in additional_attributes:
                    text = text + (str(attrib)).title() + ": "+ str(additional_attributes[attrib]) + "\n"
        except Exception as e:
            logger.error('retrieve_status.additionalattributes.exception: retrieve_location(): False - Error: {}'.format(e))
    else:
        text='There was an error retrieving status data for device: {}'.format(msg)
        logger.error('retrieve_status.retrievestatus.exception: retrieve_status(): False')
    end_user_session(text, update, context)
###### STOP STEP "STATUS"

###### START USEFUL FUNCTS
def call_traccar_api(api, context, args=None):
    if DEBUG: logger.debug('call_traccar_api: called by user {}'.format( context.user_data['uid'] ))
    if get_session(context):
        cookies = cookies = {'JSESSIONID': context.user_data['session']}
        if DEBUG: logger.debug('call_traccar_api: cookies ok {}'.format( context.user_data['session'][:-4]+'XXXX' ))
        if DEBUG: logger.debug('call_traccar_api: request api {}'.format( api ))
        if api == 'devices':
            url = TRACCAR_URL+'devices?userId='+str(context.user_data['traccar_user_id']) #GET
            r = requests.get(url, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()
            else:
                logger.error('calltraccarapi.devices.exception: status code:{}'.format( r.status_code ))
        if api == 'location':
            url = TRACCAR_URL+'positions?id='+str(args) #GET with dev_id
            r = requests.get(url, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()[0]
            else:
                logger.error('calltraccarapi.location.exception: status code:{}'.format( r.status_code ))
        if api == 'geofences':
            url = TRACCAR_URL+'geofences?deviceId='+str(args) #GET with dev_id
            r = requests.get(url, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()
            else:
                logger.error('calltraccarapi.geofences.exception: status code:{}'.format( r.status_code ))
        if api == 'create_geofences':
            url = TRACCAR_URL+'geofences' #POST with geodata in body as 'name' and 'area' (circle) (json)
            headers = {'Content-Type': 'application/json'}
            r = requests.post(url, headers=headers, data=args, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()
            else:
                logger.error('calltraccarapi.creategeofence.exception: status code:{}'.format( r.status_code ))
                return False
        if api == 'assoc_geofence':
            url = TRACCAR_URL+'permissions' #DELETE with dev_id, geo_id in Body (json) --! ORDER MATTERS!--
            headers = {'Content-Type': 'application/json'}
            r = requests.post(url, headers=headers, data=args, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 204:
                return True
            else:
                logger.error('calltraccarapi.creategeofence.exception: status code:{}'.format( r.status_code ))
                return False
        if api == 'del_geofences':
            url = TRACCAR_URL+'permissions/' #DELETE with dev_id, geo_id in Body (json) --! ORDER MATTERS!--
            headers = {'Content-Type': 'application/json'}
            r = requests.delete(url, headers=headers, data=args, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 204: #Same as 200 but with no content
                return True
            else:
                logger.error('calltraccarapi.delgeofence.exception: status code:{}'.format( r.status_code ))
                return False
        if api == 'commands':
            url = TRACCAR_URL+'commands/send?deviceId='+str(args) #GET with dev_id
            r = requests.get(url, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()
            else:
                logger.error('calltraccarapi.commands.exception: status code:{}'.format( r.status_code ))
        if api == 'send_command':
            url = TRACCAR_URL+'commands/send' #POST with dev_id in Body (json)
            headers = {'Content-Type': 'application/json'}
            r = requests.post(url, headers=headers, data=args, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return "Sent"
            elif r.status_code == 202:
                return "Queued"
            else:
                logger.error('calltraccarapi.sendcommand.exception: status code:{}'.format( r.status_code ))
        if api == 'status':
            url = TRACCAR_URL+'devices?id='+str(args) #GET with dev_id
            r = requests.get(url, cookies = cookies, timeout = TRACCAR_TIMEOUT)
            if r.status_code == 200:
                return r.json()[0]
            else:
                logger.error('calltraccarapi.status.exception: status code:{}'.format( r.status_code ))
    else:
        logger.error('calltraccarapi.getsession.exception: get_session(): False')
    return None

def get_session(context):
    if DEBUG: logger.debug('get_session: called by user {}'.format( context.user_data['uid'] ))
    now = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    fivehago = (datetime.now() - timedelta(hours=5)).strftime('%Y-%m-%dT%H:%M:%SZ')
    if 'session' in context.user_data:  #We have a session cookie, try if it's still valid
        if DEBUG: logger.debug('get_session: cookie already present')
        user_session = context.user_data['session']
        cookies = dict(name='JSESSIONID', value=user_session)
        r = requests.get(TRACCAR_URL+"statistics?from="+fivehago+"&to="+now, cookies = cookies, timeout = TRACCAR_TIMEOUT)
        if r.status_code == 200:
            if DEBUG: logger.debug('get_session: still valid session')
            return True
        else:
            if r.status_code == 401: #Unauthorized
                if DEBUG: logger.debug('get_session: session unauthorized, reauthorizing')
                return perform_auth(context)
            else:
                logger.error('getsession.checkingalready.exception: status code:{}'.format( r.status_code ))
                return False
    else:
        if DEBUG: logger.debug('get_session: no session, reauthorizing')
        return perform_auth(context)

def perform_auth(context, apikey=None):
    if DEBUG: logger.debug('perform_auth: called by user {}'.format( context.user_data['uid'] ))
    if apikey is None:
        if DEBUG: logger.debug('perform_auth: no apikey, retrieving')
        apikey = retrieve_apikey(context)
    if apikey is not None:
        if DEBUG: logger.debug('perform_auth: trying to authenticate')
        r = requests.get(TRACCAR_URL+"session?token="+apikey, timeout = TRACCAR_TIMEOUT)
        if r.status_code == 200:
            if DEBUG: logger.debug('perform_auth: authenticated!')
            context.user_data['session'] = r.cookies['JSESSIONID']
            context.user_data['traccar_user_id'] = r.json()['id']
            return True
        else:
            logger.error('getsession.performauth_apikey.exception: status code:{}'.format( r.status_code ))
    else:
        logger.error('getsession.performauth_apikey.exception: apikey not found')
    return False

def retrieve_devices(update, context):
    if DEBUG: logger.debug('retrieve_devices: called by user {}'.format( update.message.chat.id ))
    devices = call_traccar_api('devices', context)
    user_devices = []
    if devices is not None:
        if DEBUG: logger.debug('retrieve_devices: devices retrieved {}'.format( devices ))
        for device in devices:
            dev_id = device['id']
            dev_name = device['name']
            dev_ago = 'unknown'
            if device['lastUpdate'] is not None:
                try:
                    dev_ts = parser.parse(device['lastUpdate'])
                    dev_ago = abs(((datetime.now((timezone.utc)) - dev_ts).seconds) % 3600)
                except parser.ParserError:
                    logger.error('retrieve_devices.devicetime.parsererror: {}'.format( device['lastUpdate'] ))
                except TypeError:
                    logger.error('retrieve_devices.devicetime.typeerror: {}'.format( device['lastUpdate'] ))
            dev_last_position = device['positionId']
            user_devices.append({'id':dev_id, 'name':dev_name, 'last_seen':dev_ago, 'last_position':dev_last_position})
        context.user_data['devices'] = user_devices
        return True
    else:
        logger.error('retrieve_devices.retrievekeyexception.devices: none')
        update.message.reply_text(text='There was an error retrieving your your devices, try again later or contact the Traccar Admin.', reply_markup=ReplyKeyboardRemove())
    return None

def retrieve_apikey(context):
    if DEBUG: logger.debug('retrieve_apikey: called by user {}'.format( context.user_data['uid'] ))
    with sqlite3.connect(DB_HOST) as conn:
        cur = conn.cursor()
        try:
            cur.execute("SELECT traccar_apikey FROM users WHERE tg_id=?", (context.user_data['uid'],) )
            record = cur.fetchone()
            if record is not None:
                if DEBUG: logger.debug('retrieve_apikey: api retrieved from db {}'.format( record[0][:-4]+"XXXX" ))
                return record[0]
        except OperationalError as op_err:
                logger.error('retrieve_apikey.select.exception: db_err: {}, db: {}'.format( op_err, DB_HOST ))
    return None
###### STOP USEFUL FUNCTS

def handle_messages(update, context):
    if DEBUG: logger.debug('handle_messages: called by user {}'.format( update.message.chat.id ))
    """ Default handler for messages, it switch through statuses and steps in order to perform tasks """
    context.user_data['uid'] = update.message.chat.id
    msg = update.message.text
    status = context.user_data['status'] if 'status' in context.user_data else None
    step = context.user_data['step'] if 'step' in context.user_data else None
    if DEBUG: 
        if status=='new': # msg will contain apikey
            logger.debug('handle_messages: msg {}, status {}, step {}'.format( msg[:-4]+"XXXX", status, step ))
    else:
        logger.debug('handle_messages: msg {}, status {}, step {}'.format( msg, status, step ))
    if msg == 'END':
        text = 'Process canceled as user request.'
        end_user_session(text, update, context)
    else:
        if status is None:
            text = 'Invalid command or message, no status detected.'
            end_user_session(text, update, context)
        else:
            if status == 'new':
                save_apikey(update, context)
            if status == 'location':
                retrieve_location(update, context)
            if status == 'commands':
                if step == 'retrieve':
                    retrieve_commands(update, context)
                if step == 'send':
                    send_command(update, context)
            if status == 'geofences':
                if step == 'choice':
                    if msg == 'Create Geofence':
                        create_geofence(update, context)
                    if msg == 'Delete Geofences':
                        manage_geofences(update, context)
                if step == 'radius':
                    create_geofence_radius(update, context)
                if step == 'name':
                    create_geofence_name(update, context)
                if step == 'create':
                    create_geofence_send(update, context)
                if step == 'retrieve':
                    retrieve_geofences(update, context)
                if step == 'delete':
                    delete_geofence(update, context)
                if step == 'delete_confirm':
                    delete_geofence_confimation(update, context)
            if status == 'status':
                retrieve_status(update, context)

def handle_location(update, context):
    if DEBUG: logger.debug('handle_location: called by user {}'.format( update.message.chat.id ))
    context.user_data['uid'] = update.message.chat.id
    context.user_data['lat'] = update.message.location.latitude
    context.user_data['lng'] = update.message.location.longitude
    if DEBUG: logger.debug('handle_location: cgot location lat {}, lng {}'.format( update.message.location.latitude, update.message.location.longitude ))
    create_geofence_device(update, context)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", action="store", dest="conf_file", type=str, help="Config file", default="parameters.conf")
    parser.add_argument("-l", "--log", action="store", dest="log_file", type=str, help="Logging file", default="traccar-telegram-bot.log")
    parser.add_argument("-d", "--debug", help="Enable Debugging", action="store_true", default=False)
    options = parser.parse_args()

    """ Enable logging """
    log_file = options.log_file
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', filename=log_file, level=logging.INFO)
    global logger
    logger = logging.getLogger(__name__)

    """ Enable Debugging """
    global DEBUG
    DEBUG = options.debug
    if DEBUG:
        logger.setLevel('DEBUG')

    """ Retrieve configuration file """
    config = get_config(options.conf_file)

    """ Set Variables """
    tg_token = config.get('telegram', 'token')

    global DB_HOST
    DB_HOST = config.get('sqlite', 'db')

    global TRACCAR_URL
    TRACCAR_URL = config.get('traccar', 'schema')+"://"+config.get('traccar', 'host')+":"+config.get('traccar', 'port')+"/api/"

    global TRACCAR_TIMEOUT
    TRACCAR_TIMEOUT = int(config.get('traccar', 'timeout'))

    """ Print Variables """
    if DEBUG: logger.debug("main: Traccar Telegram Bot Starting with following parameters -> DB: {}, TG: {}, URL: {}, TIMEOUT: {}".format(DB_HOST, tg_token[:-8]+"XXXXXXX", TRACCAR_URL, str(TRACCAR_TIMEOUT)))

    """ Start MSGS Handling """
    try:
        updater = Updater(tg_token, use_context=True)
        dp = updater.dispatcher

        dp.add_handler(CommandHandler('start', start))
        dp.add_handler(CommandHandler('register', add_traccar_key))
        dp.add_handler(CommandHandler('location', location))
        dp.add_handler(CommandHandler('geofences', geofences))
        dp.add_handler(CommandHandler('sendcmd', commands))
        dp.add_handler(CommandHandler('status', status))
        dp.add_handler(MessageHandler(Filters.text, handle_messages))
        dp.add_handler(MessageHandler(Filters.location, handle_location))

        dp.add_error_handler(error)

        updater.start_polling()
        logger.info("Traccar Telegram Bot Started")
        updater.idle()
    except TelegramError as e:
        logger.error("Telegram token error: {}".format(e))

if __name__ == "__main__":
    main()
